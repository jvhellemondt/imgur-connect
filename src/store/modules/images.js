import api from '../../api/imgur'
import { router } from '../../main'

const state = {
  images: [],
}

const getters = {
  allImages: (state) => state.images,
}

const actions = {
  async fetchImages({
    rootState: {
      auth: { token },
    },
    commit,
  }) {
    const {
      data: { data },
    } = await api.fetchImages(token)
    commit('setImages', data)
  },
  async uploadImages(
    {
      rootState: {
        auth: { token },
      },
    },
    images
  ) {
    await api.uploadImages(images, token)
    router.push('/')
  },
}

const mutations = {
  setImages: (state, images) => (state.images = images),
}

export default {
  state,
  getters,
  actions,
  mutations,
}
