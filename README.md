State management with Vuex
Drag and drop image upload
Authentication with OAuth2

Vue, Vuex, and Vue Router

    Producing dynamic, responsive applications using Vue
    Upload images to a remote server using drag and drop image upload
    Log users into your app using OAuth2 Authentication
    Use a cutting edge project boilerplate with Vue CLI
    Reduce the amount of code you write using Template Directives
    Communicate between components using Props and Events
    Update Vue components using reactive data properties
    Progamatically navigate users around your application using Vue Router
    Model application data using the powerful Vuex framework
    Persist information stored in your app using Local Storage
    Develop a master-level understanding of the differences between imperative and declarative programming
    Learn how Vue gives developers multiple tools to accomplish task, and know which the best is for you

    Understand how to create interesting Vue applications
    Use Vuex to manage and update data stored in application state
    Navigate users between pages using Vue Router
    Authenticate users with an advanced OAuth2 flow
    Build beautiful drag and drop image upload
    Style content intelligently using CSS Grids


# images

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
